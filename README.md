# Introduction

## Release vs Feature toggle
### Release toggles
- purpose is to halt `unaccepted` stories from been released to end users
- can be automated base on story status
### Feature toggles
- purpose is to allow flexibility in our code to enable/disable certain features when needed
- have to be done manually since it involves human decisions

# Automate Release of release toggle
This repo will showcase how we are able to automatically release story once its accepted by using a ci pipeline (we could probably schedule it to run on a daily basis similar to renovateBot)

Prerequisite
- The toggle key will have to be able to identify which story its corresponding to (e.g. using pivotal story id), so as to check the story status
- code behavior when release toggle is not present should be default to `true` (as oppose to feature toggle which is default to `false`). This is so that we can clean up the toggle config json straightaway once the story is accepted instead of leaving the key there with `{production: true}`

The pipeline contains 1 job and will do the following:
1. Retrieve the release-toggle json config file which contains all the release toggles for a codebase repo (e.g. ui-employer)
2. Loop thru the toggles and retrieve the corresponsing story status from pivotal tracker
3. Remove/clean up accepted stories toggle from the json config file
4. Commit and auto merge the new json config file into its repo (e.g. ops-feature-flag)
5. Open an MR for each accepted stories in the codebase repo (e.g. ui-employer). This act as a notification/reminder/task for us to remove that particular story relase toggle from our codebase.

## For the purpose of this tech spike
- I used [demo](https://gitlab.com/mycf.sg/release-toggle-bot/-/tree/demo) branch to mimick the ops-feature-flag repo. And the auto-merged MR to update the json config file is [here](https://gitlab.com/mycf.sg/release-toggle-bot/-/merge_requests/12)

- The MRs that should be created in ui-employer is instead created [here](https://gitlab.com/mycf.sg/release-toggle-bot/-/merge_requests)

- The pipeline job that ran https://gitlab.com/mycf.sg/release-toggle-bot/-/jobs/728067973
